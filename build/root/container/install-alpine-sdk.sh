#!/bin/sh

set -e

chown -R root: /etc/sudoers.d
chmod -R u=rwX,g=rX,o= /etc/sudoers.d

apk add --no-cache --update alpine-sdk sudo

/container/user-cleanup.sh
/container/upgrade.sh

adduser -u 2001 -D abuild
addgroup abuild abuild

mkdir -p /build
mkdir -p /var/cache
chown -R abuild: /build /var/cache

rm -rf /container
